from django.shortcuts import render, redirect, get_object_or_404
from tasks.forms import TaskForm, TaskEditForm, UpdateForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task, Updates
import datetime


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/show_my_tasks.html", context)


@login_required
def show_task(request, id):
    task = get_object_or_404(Task, id=id)
    context = {
        "task": task,
    }
    return render(request, "tasks/show_task.html", context)


@login_required
def edit_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = TaskEditForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("show_task", id=id)
    else:
        form = TaskEditForm(instance=task)

    context = {
        "task": task,
        "form": form,
    }
    return render(request, "tasks/edit_task.html", context)


@login_required
def create_update(request, id):
    if request.method == "POST":
        form = UpdateForm(request.POST)
        if form.is_valid():
            update = form.save(False)
            update.updater = request.user
            update.task_id = id
            update.created = datetime.datetime.now()
            update.modified = datetime.datetime.now()
            update.save()
            return redirect("show_task", id=id)
    else:
        form = UpdateForm()

    context = {"form": form}
    return render(request, "tasks/create_update.html", context)


@login_required
def edit_update(request, id):
    update = get_object_or_404(Updates, id=id)
    if request.method == "POST":
        form = UpdateForm(request.POST, instance=update)
        if form.is_valid():
            update_form = form.save(False)
            update_form.modified = datetime.datetime.now()
            update_form.save()
            return redirect("show_task", id=update.task_id)
    else:
        form = UpdateForm(instance=update)

    context = {
        "update": update,
        "form": form,
    }
    return render(request, "tasks/edit_update.html", context)
