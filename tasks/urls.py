from django.urls import path
from tasks.views import (
    create_task,
    show_my_tasks,
    show_task,
    edit_task,
    create_update,
    edit_update,
)

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:id>/", show_task, name="show_task"),
    path("<int:id>/edit/", edit_task, name="edit_task"),
    path("<int:id>/create_update/", create_update, name="create_update"),
    path("update/<int:id>/edit/", edit_update, name="edit_update"),
]
