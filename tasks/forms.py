from django import forms
from tasks.models import Task, Updates
from django.forms import ModelForm
from django.contrib.admin.widgets import AdminSplitDateTime
from ckeditor.widgets import CKEditorWidget


class TaskForm(ModelForm):
    due_date = forms.SplitDateTimeField(widget=AdminSplitDateTime())
    start_date = forms.SplitDateTimeField(widget=AdminSplitDateTime())

    class Meta:
        model = Task
        fields = "__all__"


class TaskEditForm(ModelForm):
    due_date = forms.SplitDateTimeField(widget=AdminSplitDateTime())

    class Meta:
        model = Task
        exclude = (
            "start_date",
            "project",
            "assignee",
        )


class TaskCompletedForm(ModelForm):
    class Meta:
        model = Task
        fields = ("is_completed",)


class UpdateForm(ModelForm):
    notes = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Updates
        fields = (
            "title",
            "notes",
        )
