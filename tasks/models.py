from django.db import models
from projects.models import Project
from django.conf import settings
from ckeditor.fields import RichTextField



# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField(null=True)
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Updates(models.Model):
    created = models.DateTimeField()
    modified = models.DateTimeField()
    title = models.CharField(max_length=240)
    notes = RichTextField()
    updater = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="updates",
        on_delete=models.CASCADE,
    )
    task = models.ForeignKey(
        Task,
        related_name="updates",
        on_delete=models.CASCADE,
    )
